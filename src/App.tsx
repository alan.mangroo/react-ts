import React from 'react';
import './App.css';
import Header from './header';

function App() {
  return (
    <div className="App">
        <Header title={'Hello World' } colour={'red'} />
    </div>
  );
}

export default App;

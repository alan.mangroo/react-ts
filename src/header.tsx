export interface Props {
    title: string
    colour? : string
}

const Header = (props: Props) => {
    return(
        <header>
            <h1 style={{color: props.colour ? props.colour : 'blue'}}>{props.title}</h1>
        </header>
    )
}

export default Header